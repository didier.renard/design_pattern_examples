import java.util.List;
import java.util.ArrayList;

interface ICommand
{
    String name();
    void run(String[] args);
}

interface IInterpreter
{
    void addCommand(ICommand command);
    void runCommand(String commandName, String[] args);
}

class PrintCommand implements ICommand
{
    @Override
    public String name()
    {
        return "print";
    }

    @Override
    public void run(String[] args)
    {
        for (String arg : args)
        {
            System.out.print(arg + " ");
        }
        System.out.println();
    }
}

public class Interpreter implements IInterpreter
{
    @Override
    public void addCommand(ICommand command)
    {
        this.commands.add(command);
    }

    @Override
    public void runCommand(String commandName, String[] args)
    {
        for (ICommand command : this.commands)
        {
            if (command.name().equals(commandName))
            {
                command.run(args);
            }
        }
    }

    public static void main(String[] args)
    {
        Interpreter i = new Interpreter();
        i.addCommand(new PrintCommand());
        i.runCommand("print", new String[] { "Hola", "charola" });
    }

    private List<ICommand> commands = new ArrayList<ICommand>();
}
