import java.util.ArrayList;

public class AgeOfEmpiresArmy implements AgeOfEmpiresArmyComponent
{
    public AgeOfEmpiresArmy()
    {
        this.armyComponents = new ArrayList<AgeOfEmpiresArmyComponent>();
    }

    @Override
    public String name()
    {
        return "Army";
    }

    public void addArmyComponent(AgeOfEmpiresArmyComponent component)
    {
        this.armyComponents.add(component);
    }

    @Override
    public int count()
    {
        int total = 0;
        for (AgeOfEmpiresArmyComponent component : this.armyComponents)
        {
            total += component.count();
        }
        return total;
    }

    @Override
    public void setCount(int count)
    {
        System.out.println("You need to specify the component; use ::setCount(String, int)");
    }

    public void setCount(String component, int count)
    {
        AgeOfEmpiresArmyComponent comp = this.component(component);
        if (comp != null)
        {
            comp.setCount(count);
        }
    }

    public AgeOfEmpiresArmyComponent component(String named)
    {
        for (AgeOfEmpiresArmyComponent component : this.armyComponents)
        {
            if (component.name().equals(named))
            {
                return component;
            }
        }

        return null;
    }

    @Override
    public void attack()
    {
        for (AgeOfEmpiresArmyComponent component : this.armyComponents)
        {
            component.attack();
        }
    }

    public void attack(String withComponent)
    {
        AgeOfEmpiresArmyComponent component = this.component(withComponent);
        if (component != null)
        {
            component.attack();
        }
        else
        {
            System.out.println("No such army: " + withComponent);
        }
    }

    private ArrayList<AgeOfEmpiresArmyComponent> armyComponents;
}