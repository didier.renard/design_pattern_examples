
public interface AgeOfEmpiresArmyComponent
{
    String name();
    int count();
    void setCount(int count);
    void attack();
}