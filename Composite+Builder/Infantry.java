public class Infantry extends AgeOfEmpiresArmyComponentBase
{
    public Infantry(int count)
    {
        super(count);
    }

    @Override
    public String name()
    {
        return "Infantry";
    }

    @Override
    public void attack()
    {
        System.out.println("Sword attack");
    }
}