
public class Ranged extends AgeOfEmpiresArmyComponentBase
{
    public Ranged(int count)
    {
        super(count);
    }
    
    @Override
    public String name()
    {
        return "Ranged";
    }

    @Override
    public void attack()
    {
        System.out.println("Firing arrows");
    }
}