
public class Cavalry extends AgeOfEmpiresArmyComponentBase
{
    public Cavalry(int count)
    {
        super(count);
    }

    @Override
    public String name()
    {
        return "Cavalry";
    }

    @Override
    public void attack()
    {
        System.out.println("Palas OP");
    }
}