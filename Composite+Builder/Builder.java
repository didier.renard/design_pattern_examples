class AgeOfEmpiresArmyBuilder
{
    public AgeOfEmpiresArmyBuilder()
    {
        this.building = new AgeOfEmpiresArmy();
    }

    public AgeOfEmpiresArmyBuilder addInfantry(int count)
    {
        this.building.addArmyComponent(new Infantry(count));
        return this;
    }

    public AgeOfEmpiresArmyBuilder addRanged(int count)
    {
        this.building.addArmyComponent(new Ranged(count));
        return this;
    }

    public AgeOfEmpiresArmyBuilder addSiege(int count)
    {
        this.building.addArmyComponent(new Siege(count));
        return this;
    }

    public AgeOfEmpiresArmyBuilder addCavalry(int count)
    {
        this.building.addArmyComponent(new Cavalry(count));
        return this;
    }

    public AgeOfEmpiresArmy get()
    {
        AgeOfEmpiresArmy ret = this.building;
        this.building = new AgeOfEmpiresArmy();
        return ret;
    }

    private AgeOfEmpiresArmy building;
}

public class Builder
{
    public static void main(String[] args)
    {
        AgeOfEmpiresArmyBuilder b = new AgeOfEmpiresArmyBuilder();

        AgeOfEmpiresArmy army = b.addInfantry(10).addRanged(10).addSiege(2).addCavalry(4).get();
    }
}
