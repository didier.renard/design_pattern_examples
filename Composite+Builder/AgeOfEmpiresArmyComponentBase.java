public abstract class AgeOfEmpiresArmyComponentBase implements AgeOfEmpiresArmyComponent
{
    public AgeOfEmpiresArmyComponentBase(int count)
    {
        this.setCount(count);
    }

    @Override
    public int count()
    {
        return this.count;
    }

    @Override
    public void setCount(int count)
    {
        this.count = count;
    }

    private int count;
}