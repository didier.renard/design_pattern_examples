public class Composite
{
    public static void main(String[] args)
    {
        AgeOfEmpiresArmy army = new AgeOfEmpiresArmy();

        army.addArmyComponent(new Infantry(10));
        army.addArmyComponent(new Ranged(10));
        army.addArmyComponent(new Siege(2));
        army.addArmyComponent(new Cavalry(4));

        army.attack("Infantry");
        army.attack("Ranged");
        army.attack("Monks");
        army.attack("Siege");
        army.attack("Cavalry");
        army.attack("Gun powder");
    }
}
