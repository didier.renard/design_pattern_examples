
public class Siege extends AgeOfEmpiresArmyComponentBase
{
    public Siege(int count)
    {
        super(count);
    }

    @Override
    public String name()
    {
        return "Siege";
    }

    @Override
    public void attack()
    {
        System.out.println("Mango shot");
    }
}