interface Car
{
    void start();
    void stop();
    void gas();
    void brake();
}

interface CarFactory
{
    Car create();
}

class ClassicCar implements Car
{
    public ClassicCar()
    {
        System.out.println("Car has been constructed");
    }

    @Override
    public void gas()
    {
        System.out.println("Hitting the gas pedal");
    }

    @Override
    public void brake()
    {
        System.out.println("Hitting the brakes");
    }

    @Override
    public void stop()
    {
        System.out.println("Engine went off");
    }

    @Override
    public void start()
    {
        System.out.println("Engine started");
    }
}

class ClassicCarFactory implements CarFactory
{
    @Override
    public Car create()
    {
        return new ClassicCar();
    }
}

class CarProxy implements Car
{
    public CarProxy(CarFactory factory)
    {
        this.factory = factory;
    }

    @Override
    public void start()
    {
        this.checkCarInstance();
        this.car.start();
    }

    @Override
    public void stop()
    {
        this.checkCarInstance();
        this.car.stop();
    }

    @Override
    public void gas()
    {
        this.checkCarInstance();
        this.car.gas();
    }

    @Override
    public void brake()
    {
        this.checkCarInstance();
        this.car.brake();
    }

    private void checkCarInstance()
    {
        if (this.car == null)
        {
            this.car = this.factory.create();
        }
    }

    private Car car;
    private CarFactory factory;
}

public class Proxy
{
    public static void main(String[] args)
    {
        CarFactory factory = new ClassicCarFactory();
        Car car = new CarProxy(factory);
        Car car2 = new CarProxy(factory);

        car.start();
        car.gas();
        car.brake();
        car.stop();
    }
}
