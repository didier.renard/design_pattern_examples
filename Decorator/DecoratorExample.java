interface IPerson
{
    String talk();
}

class Person implements IPerson
{
    public Person()
    {
    }

    @Override
    String talk()
    {
        return "Hello";
    }
}

interface IPersonDecorator
{
    Person person();
}

abstract class PersonDecoratorBase implements IPersonDecorator, IPerson
{
    public PersonDecoratorBase(Person person)
    {
        this.person = person;
    }

    @Override
    public Person person()
    {
        return this.person;
    }

    private Person person;
}

class SoldierDecorator extends PersonDecoratorBase
{
    public SoldierDecorator(Person person)
    {
        super(person);
    }
    
    @Override
    public String talk()
    {
        return this.person().talk();
    }

    public void fire()
    {
        System.out.println("Ratatatatatatata");
    }
}

public class DecoratorExample
{
    public static void main(String[] args)
    {
        IPerson p = new Person();
        SoldierDecorator s = new SoldierDecorator(p);
        p.talk();
        p = s;
        p.talk();
        s.person().talk();
        s.talk();
        s.fire();
    }
}
