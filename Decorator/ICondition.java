
public interface ICondition
{
    boolean met();
}

