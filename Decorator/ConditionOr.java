import java.util.List;

public class ConditionOr implements ICondition
{
    public ConditionOr(List<ICondition> conditions)
    {
        super(conditions);
    }

    @Override
    public boolean met()
    {
        for (ICondition condition : this.conditions)
        {
            if (condition.met())
            {
                return true;
            }
        }

        return false;
    }
}
