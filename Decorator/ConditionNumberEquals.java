
public class ConditionNumberEquals implements ICondition
{
    public ConditionNumberEquals(int whichNumber, int equalsThisOne)
    {
        this.number = whichNumber;
        this.equalsTo = equalsThisOne;
    }

    @Override
    public boolean met()
    {
        return this.number == this.equalsTo;
    }

    private int number;
    private int equalsTo;
}

