import java.util.List;

public class ConditionAnd extends ConditionMultiple
{
    public ConditionAnd(List<ICondition> conditions)
    {
        super(conditions);
    }

    @Override
    public boolean met()
    {
        for (ICondition condition : this.conditions)
        {
            if (!condition.met())
            {
                return false;
            }
        }

        return true;
    }
}
