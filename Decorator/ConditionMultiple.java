import java.util.List;

public abstract class ConditionMultiple implements ICondition
{
    public ConditionMultiple(List<ICondition> conditions)
    {
        this.conditions = conditions;
    }

    private List<ICondition> conditions;
}
