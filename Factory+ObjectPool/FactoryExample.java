
public class FactoryExample
{
    public static void main(String[] args)
    {
        WorkerFactory wf = new WorkerFactory();
        wf.create(10, 10);
    }
}
