import java.util.List;
import java.util.ArrayList;

public class WorkerPool
{
    public WorkerPool(WorkerFactory factory, int poolSize)
    {
        for (int i = 0; i < poolSize; i++)
        {
            this.available.add(factory.create((i + 1) * 100, (i + 1) * 100));
        }
    }

    public Worker take()
    {
        if (this.available.size() > 0)
        {
            Worker w = this.available.get(0);
            this.available.remove(w);
            this.unavailable.add(w);
            return w;
        }

        return null;
    }

    public void free(Worker worker)
    {
        for (Worker w : this.unavailable)
        {
            if (w == worker)
            {
                this.unavailable.remove(w);
                this.available.add(w);
                break;
            }
        }
    }

    private List<Worker> available = new ArrayList<Worker>();
    private List<Worker> unavailable = new ArrayList<Worker>();
}
