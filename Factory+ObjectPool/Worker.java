
public class Worker
{
    public int weight()
    {
        return this.weight;
    }

    public int height()
    {
        return this.height;
    }

    public void setWeight(int weight)
    {
        this.weight = weight;
    }

    public void setHeight(int height)
    {
        this.height = height;
    }

    private int weight = 0;
    private int height = 0;
}