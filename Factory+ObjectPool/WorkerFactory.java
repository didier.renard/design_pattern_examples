
public class WorkerFactory
{
    public Worker create(int height, int weight)
    {
        Worker s = new Worker();
        s.setHeight(height);
        s.setWeight(weight);
        return s;
    }
}
