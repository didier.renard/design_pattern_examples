
public class Main
{
    public static void main(String[] args)
    {
        Library l = new Library();
        l.addBook(new Book("Yo", "nomeacuerdo", "Lo que no te contaron de..."));
        l.addBook(new Book("Luis", "seguroellosabe", "Algo de psico"));

        System.out.println(l.findBookByIsbn("nomeacuerdo"));
        System.out.println(l.findBookByIsbn("dondeestamiamiga"));
    }
}
