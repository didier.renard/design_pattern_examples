
public abstract class AbstractBook implements IBook
{
    @Override
    public String toString()
    {
        return "\"" + this.title() + "\", " + this.author() + " (" + this.isbn() + ")";
    }
}
