
public class NullBook extends AbstractBook
{
    @Override
    public String author()
    {
        return "null";
    }
    
    @Override
    public String isbn()
    {
        return "null";
    }

    @Override
    public String title()
    {
        return "null";
    }
}
