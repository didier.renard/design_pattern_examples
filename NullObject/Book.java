
public class Book extends AbstractBook
{
    public Book(String author, String isbn, String title)
    {
        this.author = author;
        this.isbn = isbn;
        this.title = title;
    }

    @Override
    public String author()
    {
        return this.author;
    }

    @Override
    public String isbn()
    {
        return this.isbn;
    }

    @Override
    public String title()
    {
        return this.title;
    }

    private String author;
    private String isbn;
    private String title;
}

