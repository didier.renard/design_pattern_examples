

public interface IBook
{
    String author();
    String isbn();
    String title();
}
