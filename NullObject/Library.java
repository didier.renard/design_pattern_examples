import java.util.List;
import java.util.ArrayList;

public class Library
{
    public Library(IBook nullBook)
    {
        this.books = new ArrayList<IBook>();
        this.nullBook = nullBook;
    }

    public Library()
    {
        this(new NullBook());
    }
    
    public void addBook(IBook book)
    {
        this.books.add(book);
    }

    public IBook findBookByIsbn(String isbn)
    {
        for (IBook book : this.books)
        {
            if (book.isbn().equalsIgnoreCase(isbn))
            {
                return book;
            }
        }

        return this.nullBook;
    }

    private List<IBook> books;
    private IBook nullBook;
}
