import java.util.List;
import java.util.ArrayList;
import java.lang.Thread;

interface IRecipe
{
    String name();
}

class Milanesa implements IRecipe
{
    @Override
    public String name()
    {
        return "Milanga";
    }
}

interface IOvenListener
{
    void cookingStarted(Oven sender, IRecipe meal);
    void cookingCompleted(Oven sender, IRecipe meal);
}

class Oven
{
    public void cook(IRecipe meal)
    {
        System.out.println("Cooking " + meal.name() + "...");

        for (IOvenListener listener : this.listeners)
        {
            listener.cookingStarted(this, meal);
        }

        try
        {
            Thread.sleep(5000);
        }
        catch(InterruptedException e)
        {
        }
        System.out.println("Cooked!");

        for (IOvenListener listener : this.listeners)
        {
            listener.cookingCompleted(this, meal);
        }
    }

    public void addListener(IOvenListener listener)
    {
        this.listeners.add(listener);
    }

    private List<IOvenListener> listeners = new ArrayList<IOvenListener>();
}

class Person implements IOvenListener
{
    public void eat(IRecipe meal)
    {
        System.out.println("Eating " + meal.name());
    }

    @Override
    public void cookingStarted(Oven sender, IRecipe meal)
    {
        System.out.println("This person will play AoE II DE while waiting for the " + meal.name() + " to be cooked");
    }

    @Override
    public void cookingCompleted(Oven sender, IRecipe meal)
    {
        eat(meal);
    }
}

public class ObserverExample
{
    public static void main(String[] args)
    {
        Oven o = new Oven();
        Person me = new Person();
        o.addListener(me);
        IRecipe theMila = new Milanesa();
        o.cook(theMila);
    }
}
